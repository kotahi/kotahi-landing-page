// get peertube vids and data!
// {{ videoid or UUID | peertube | safe }}

const { default: axios } = require("axios");

module.exports = function(eleventyConfig, options) {
  eleventyConfig.addFilter("peertube", async function(videoID) {
    const video = await getvid(videoID);
    return ` <iframe width="560"
      height="315"
      sandbox="allow-same-origin allow-scripts allow-popups"
      title="${video.title}"
      src="https://peertube.coko.foundation/videos/embed/${video.uuid}"
      frameborder="0"
      allowfullscreen></iframe>`;
  });

  eleventyConfig.addFilter("formattime", function(data) {
    return formatTime(data);
  });

  eleventyConfig.addCollection("kotahivids", async function() {
    const videos = await getVideoFromPlaylist(7);
    return videos;
  });
};

// get vids from coko peertubes
async function getvids() {
  const res = await axios.get(
    `https://peertube.coko.foundation/api/v1/videos/`,
  );
  return res.data.data;
}

// get one vid from coko peertubes
async function getvid(id) {
  const res = await axios
    .get(`https://peertube.coko.foundation/api/v1/videos/${id}`)
    .then((response) => {
      return res.data;
    })
    .catch((err) => console.log(err));
}

/*format time*/
function formatTime(seconds) {
    // Calculate hours, minutes, and remaining seconds
    var hours = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds % 3600) / 60);
    var remainingSeconds = seconds % 60;

    // Build the formatted time string
    var formattedTime = "";

    if (hours > 0) {
        formattedTime += hours + ":";
    }

    if (minutes > 0 || formattedTime !== "") {
        formattedTime += (formattedTime !== "" ? "" : "") + ((minutes < 10 && formattedTime !== "") ? "0" + minutes : minutes) + ":";
    }

    if (remainingSeconds > 0 || formattedTime === "") {
        formattedTime += (formattedTime !== "" ? "" : "") + ((remainingSeconds < 10) ? "0" + remainingSeconds : remainingSeconds) + "s";
    }

    return formattedTime;
}

async function getVideoFromPlaylist(playlistId) {
  // https://peertube.coko.foundation/api/v1/video-playlists/7/videos

  const res = await axios.get(
    `https://peertube.coko.foundation/api/v1/video-playlists/${playlistId}/videos`,
  );
  let vidCollection = [];
  res.data.data.forEach((vid) => {
    vidCollection.push(vid.video);
  });
  // const videocollections =
  return vidCollection;
}
