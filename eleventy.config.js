const blockEmbedPlugin = require("markdown-it-block-embed");
const frame = require("markdown-it-iframe");
const flatten = require("flat");
const featureTable = require("./plugins/featureTable.js");
const gitlabreleases = require("./plugins/gitlabreleases.js");
const addId = require("./plugins/addID.js");
const html5Embed = require("markdown-it-html5-embed");
const implicitFigures = require("markdown-it-implicit-figures");
const markdownIt = require("markdown-it");
const pluginTOC = require("eleventy-plugin-nesting-toc");
const lightningCSS = require("@11tyrocks/eleventy-plugin-lightningcss");
// const eleventyPeertube = require("./plugins/eleventy-peertube.js");

module.exports = function(eleventyConfig) {
  // plugins

  eleventyConfig.addPlugin(gitlabreleases);
  // eleventyConfig.addPlugin(eleventyPeertube);

  eleventyConfig.addPlugin(pluginTOC, {
    tags: ["h2", "h3", "h4"], // which heading tags are selected headings must each have an ID attribute
    wrapper: "nav", // element to put around the root `ol`/`ul`
    wrapperClass: "toc", // class for the element around the root `ol`/`ul`
    ul: false, // if to use `ul` instead of `ol`
    flat: false,
  });

  eleventyConfig.addPlugin(featureTable);
  eleventyConfig.addPlugin(addId);

  //lightningcss
  eleventyConfig.addPlugin(lightningCSS);

  eleventyConfig.addFilter("prependLinks", function(value, prepend) {
    let regex = /<a href="/g;
    return value.replace(regex, `<a href="${prepend}`);
  });

  eleventyConfig.addCollection("book", function(collectionApi) {
    return collectionApi

      .getFilteredByGlob("./src/content*/books/kotahi/chapters/*.md", "./src/content*/books/kotahi/chapters/*.html")
      .sort(function(a, b) {
        return a.data.order - b.data.order;
      });
  });

  eleventyConfig.addCollection("sortedByOrder", function(collectionApi) {
    return collectionApi.getAll().sort(function(a, b) {
      return a.data.order - b.data.order;
    });
  });

  eleventyConfig.addCollection("news", function(collectionApi) {
    return collectionApi.getFilteredByGlob("./src/content/blog/entries/*.md");
  });
  eleventyConfig.addCollection("featuresTable", function(collectionApi) {
    return collectionApi.getFilteredByGlob(
      "./src/content/features/featureList.md",
    );
  });

  eleventyConfig.addCollection("menuitems", function(collectionApi) {
    // create a collection for everything that need to be in the menu, using the menutitle metadata

    let collection = collectionApi

      .getFilteredByGlob("./src/content/**/*.md")
      .filter(function(item) {
        if (item.data.menu) {
          return item;
        }
      })
      .sort(function(a, b) {
        return a.data.order - b.data.order;
      });
    return collection;
  });

  // create a collection for all the page that have menufooter in there frontmatter
  eleventyConfig.addCollection("menufooteritems", function(collectionApi) {
    let collection = collectionApi
      .getFilteredByGlob("./src/content/**/*.md", "./src/content/**/*.html")
      .filter(function(item) {
        if (item.data.menufooter) {
          return item;
        }
      })
      .sort(function(a, b) {
        return b.order - a.order;
      });
    return collection;
  });

  // files copy
  eleventyConfig.addPassthroughCopy({ "static/css": "css" });
  eleventyConfig.addPassthroughCopy({ "static/js": "js" });
  eleventyConfig.addPassthroughCopy({ "static/admin": "admin" });
  eleventyConfig.addPassthroughCopy({ "static/images": "images" });
  eleventyConfig.addPassthroughCopy({ "static/font": "font" });
  eleventyConfig.addPassthroughCopy({ "static/images/favicons": "/" });

  // packages for markdown
  let markdownLib = markdownIt({ html: true })
    .use(implicitFigures, {
      dataType: false, // <figure data-type="image">, default: false
      figcaption: false, // <figcaption>alternative text</figcaption>, default: false
      tabindex: true, // <figure tabindex="1+n">..., default: false
      // link: true, // <a href="img.png"><img src="img.png"></a>, default: false
    })
    .use(frame)
    .use(blockEmbedPlugin, {
      containerClassName: "video-embed",
      outputPlayerSize: false,
    })
    .use(html5Embed, {
      html5embed: {
        useImageSyntax: true, // Enables video/audio embed with ![]() syntax (default)
        useLinkSyntax: true, // Enables video/audio embed with []() syntax
      },
    });
  eleventyConfig.setLibrary("md", markdownLib);

  eleventyConfig.addFilter("markdownify", (value) => markdownLib.render(value));
  eleventyConfig.addFilter("markdownifyInline", (value) =>
    markdownLib.renderInline(value),
  );

  eleventyConfig.addFilter("showAvailableMeta", function(value) {
    return propertiesToArray(value);
  });

  eleventyConfig.addFilter("fulldate", (value) => {
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    const date = new Date(value);
    if (isNaN(date)) return value;
    return Intl.DateTimeFormat("en-GB", options).format(date);
  });

  eleventyConfig.addFilter("urldate", (value) => {
    const date = new Date(value);
    if (isNaN(date)) return value;
    return date.toISOString().split("T")[0];
  });
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};

function propertiesToArray(value) {
  let stuff = flatten(value, { maxDepth: 10 });
  let content = "";
  for (var key in stuff) {
    content += `<section><div class="meta">${key}</div><div class="value">${stuff[key] != null ? stuff[key] : ""
      }</div></section>`;
  }
  return content;
}
