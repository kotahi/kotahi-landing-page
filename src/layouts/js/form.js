var form = document.querySelector("#requestDemo");
form?.addEventListener("submit", sendMail, true);

function sendMail(event) {
  event.preventDefault();

  const mail = document.querySelector("#mail").value;
  const name = document.querySelector("#name").value;

  if (document.querySelector("#gotcha").value == "") {
    var data = {
      name: name,
      email: mail,
    };

    // smtp(name, mail);

    // openpublishingfeststrapi is off now
    axios({
      url: "https://openpublishingfest2.cloud68.co/api/kotahi-demos",
      method: "POST",
      data: { data },
      headers: {
        "Content-Type": "application/vnd.api+json",
      },
    })
      // .post(`https://openpublishingfest2.cloud68.co/api/kotahi-demo`, { data }, {})
      .then(function (response) {
        console.log(response);
        document.querySelector("#modal .content").innerHTML =
          `<p class="thanks">Thanks for your interest, we’ll contact you soon!</p>`;
      })
      .catch((error) => {
        document.querySelector("#modal .content").innerHTML = `
                  <p class="thanks">An error occured: ${error}</p>
                  <p class="thanks">There has been an error while uploading your form, please contact <a href="mailto:adam@coko.foundation">adam @ coko . foundation</a>.</p> `;
        scrollTo(document.querySelector("#modal"));
      });
  }
}
// to check for badly formatted mail

function emailIsValid(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

function checkMail(mail) {
  if (!emailIsValid(mail)) {
    alert(
      "There is a problem with your email adress. It should look like this: awesome@domain.my",
    );
  }
}

// const dialog = document.querySelector("#modal");
// const showButton = document.querySelector("#openDialog");
// const closeButton = document.querySelector("#closeRequest");

//open the modal
// showButton?.addEventListener("click", () => {
// dialog.showModal();
// });

// close the modal
// closeButton.addEventListener("click", () => {
// dialog.close();
// });
