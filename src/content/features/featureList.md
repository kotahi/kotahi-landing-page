---
permlaink: false
---
{% featureTable %}
{% featureTitle "Upcoming Development" %}
{% feature "Inbrowser CMS CSS Editor" %}
{% feature "Download Submission JSON" %}
{% feature "Author Proofing Workflow (v1)" %}
{% feature "Improved Form Builder UI" %}
{% feature "Inbrowser CMS Template Editor" %}
{% feature "Researcher-publishing architype (beta)", "24q1" %}
{% feature "Collection management", "24q1" %}
{% feature "CMS File Tree management", "24q1" %}
{% feature "Collaborative Reviews", "24q1" %}
{% feature "1 Minute Migration (beta)", "24q1" %}
{% feature "AI PDF Designer (beta)", "24q1" %}
{% feature "Plugin architecture" , "24q2" %}
{% feature "Concurrent editing", "24q2" %}
{% feature "ROR" , "24q2" %}
{% feature "DataCite DOI" , "24q2" %}

{% featureTitle "System" %}
{% feature "End to End Solution" %}
{% feature "Single Source Publishing" %}
{% feature "Multitenancy (seperate groups)" %}
{% feature "Configureable workflows" %}
{% feature "S3 compatible storage" %}
{% feature "Integrated dashboards" %}
{% feature "Role based" %}
{% feature "Open Source" %}
{% feature "Single Login for all roles" %}

{% featureTitle "Admin" %}
{% feature "User management" %}
{% feature "Customisable submission forms" %}
{% feature "Customisable review forms" %}
{% feature "Customisable decision forms" %}
{% feature "Editorial team management" %}
{% feature "Customisable workflows" %}
{% feature "Frontend configuration manager" %}
{% feature "Customisable manuscripts table display" %}
{% feature "User interface language translation" %}

{% featureTitle "Workflows" %}
{% feature "Journal workflows" %}
{% feature "Preprint reviews" %}
{% feature "PRC workflows" %}
{% feature "Conference proceedings workflows" %}
{% feature "Review-only workflow" %}
{% feature "Submission-only workflow" %}
{% feature "Production-only workflow" %}
{% feature "Micropubs workflows" %}
{% feature "Arbitary workflows (book reviews, funding proposals etc)" %}

{% featureTitle "Submission" %}
{% feature "Author dashboard" %}
{% feature "User profile pages" %}
{% feature "Automatic ingestion" %}
{% feature "Manual submission" %}
{% feature "Ad-hoc submission" %}
{% feature "Invited submissions" %}
{% feature "Author invitation" %}
{% feature "Attachments" %}
{% feature "URL submission support" %}
{% feature "Inbrowser article editing" %}
{% feature "Citation parsing" %}
{% feature "Docx ingestion" %}
{% feature "Submission attachments (LaTeX, zip files etc)" %}
{% feature "Support data attachments" %}
{% feature "Validate DOIs on submission/import" %}

{% featureTitle "Review" %}
{% feature "Reviewer Dashboard" %}
{% feature "Customisable review process" %}
{% feature "Double blind" %}
{% feature "Single blind" %}
{% feature "Open peer review" %}
{% feature "Shared review writing" %}
{% feature "Independent review writing" %}

{% featureTitle "Editorial" %}
{% feature "Editorial dashboard" %}
{% feature "Tokenised reviewer invitations" %}
{% feature "Tokenised author Invitations" %}
{% feature "Manuscript sorting" %}
{% feature "Customisable taxonomy" %}
{% feature "Peer review cycle overview dashboard" %}
{% feature "Versioned manuscript and metadata" %}
{% feature "Archiving submissions" %}
{% feature "State indicators" %}
{% feature "Manuscrupt grouping" %}
{% feature "Simple search" %}
{% feature "Advanced expression search" %}

{% featureTitle "Production" %}
{% feature "Push button JATS export" %}
{% feature "Push button PDF export" %}
{% feature "Push Button HTML Export" %}
{% feature "Automated typesetting" %}
{% feature "Copy editing" %}
{% feature "Author proofs" %}
{% feature "Corrections" %}
{% feature "Semantic tagging (XML markup)" %}
{% feature "Automated citation parser"  %}
{% feature "Inbrowser PDF CSS Editor" %}
{% feature "Inbrowser PDF Template Editor" %}
{% feature "Math Support" %}

{% featureTitle "Publishing" %}
{% feature "Customisable publishing" %}
{% feature "Easily 'select and publish' form field data" %}
{% feature "Publish anywhere" %}
{% feature "Staged publishing" %}
{% feature "Integrated CMS (FLAX)" %}
{% feature "CMS (FLAX) management UI" %}

{% featureTitle "Notifications and communications" %}
{% feature "Live chat (editorial team)" %}
{% feature "Live chat (with author)" %}
{% feature "Chat moderation (editing / deleting)" %}
{% feature "@ mentions" %}
{% feature "Muting of notifications" %}
{% feature "Video calls (editorial team)" %}
{% feature "Email notifications" %}
{% feature "Event notifications" %}
{% feature "Task notifications" %}
{% feature "Presence indicators" %}
{% feature "Interface to create / customise email notification templates" %}
{% feature "Threaded discussions" %}

{% featureTitle "Article Editing" %}
{% feature "Math support" %}
{% feature "Semantic markup" %}
{% feature "Citation parsing" %}
{% feature "Tables" %}
{% feature "Images" %}
{% feature "Comments" %}
{% feature "Track changes" %}
{% feature "Asset manager" %}

{% featureTitle "Task Management" %}
{% feature "Task manager" %}
{% feature "Customisable Task Templates" %}
{% feature "Duration and Date Tracking" %}

{% featureTitle "Reports" %}
{% feature "Realtime reports" %}
{% feature "Summary" %}
{% feature "By role" %}
{% feature "Date range filtering" %}
{% feature "Status board" %}

{% featureTitle "Integrations" %}
{% feature "Import API" %}
{% feature "Export API" %}
{% feature "Sciety" %}
{% feature "bioRxiv" %}
{% feature "medRxiv" %}
{% feature "PubMed" %}
{% feature "EuropePMC" %}
{% feature "Semantic Scholar" %}
{% feature "ORCID" %}
{% feature "CrossRef" %}
{% feature "Hypothesis" %}
{% feature "COAR notify" %}
{% feature "DocMaps" %}
{% feature "SSO Support" %}
{% feature "FLAX (Kotahi CMS)" %}

{% featureTitle "Stack" %}
{% feature "Node" %}
{% feature "React" %}
{% feature "Expressjs" %}
{% feature "GraphQL" %}
{% feature "Postgres" %}
{% feature "Minio" %}
{% feature "CokoServer" %}

{% featureTitle "Tests" %}
{% feature "Continuous Integration" %}
{% feature "Cypress" %}

{% featureTitle "Deployment" %}
{% feature "Docker" %}

{% featureTitle "Microservices" %}
{% feature "XSweet (MS Word conversion)" %}
{% feature "Pagedjs (PDF rendering)" %}
{% feature "Anystyle (Citation parser)" %}
{% feature "FLAX (Publishing frontend)" %}
{% endfeatureTable %}

