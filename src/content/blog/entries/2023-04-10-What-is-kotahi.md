---
tags: news
date: 2023-04-04T15:38:25.688Z
author: Ryan Dix-Peek
title: "What is Kotahi?"
intro: Kotahi means 'unified' in te reo Māori, symbolising the community
  collaboration of improving how research is shared and innovating new ways of
  publishing.
---

Kotahi is an innovative and collaborative scholarly publishing platform that allows researchers, reviewers, and editors to work seamlessly together, creating and distributing high-quality research with ease. With its state-of-the-art open source technologies, Kotahi provides a comprehensive suite of powerful tools and features for content creation and ingestion, reviewing, editing, and distribution, making it the go-to solution for publishers seeking to streamline their workflows and reduce costs or to experiment with entirely new ways of working.

Kotahi means 'unified' in te reo Māori, symbolising the community collaboration of improving how research is shared and innovating new ways of publishing.

## Embrace the future with Kotahi: a Web-First vision

What distinguishes Kotahi from the rest? It's simple – Kotahi is the only truly 'web-first' scholarly publishing platform available today.

While most platforms rely on traditional back-office tracking systems designed in a print-era world to manage files (such as MS Word/LaTeX), Kotahi boldly steps into the future by bringing scholarly workflows to the web. By embracing collaboration, concurrency, real-time communication, version control, automated typesetting, drag-and-drop configuration, AI assistance, and flexible workflows, Kotahi revolutionises the scholarly publishing landscape.

## The power of open source

Welcome to the world of open source software, where innovation and collaboration reign supreme. At Coko, we firmly believe that working together is the key to unlocking the full potential of technology. That's why every piece of software we create, including Kotahi, is 100% open source.

Open source goes beyond simply the source code, by joining forces with the wider community, we can all tap into a wealth of knowledge and expertise that we wouldn't have access to otherwise. This collaborative approach means we can build better products more efficiently, with fewer errors, and at a lower cost than traditional proprietary software.
