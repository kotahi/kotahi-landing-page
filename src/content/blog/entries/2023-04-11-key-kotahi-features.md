---
layout: newsSingle.njk
tags: news
date: 2023-04-11T02:35:30.007Z
author: Adam Hyde, Paul Shannon, Ben Whitemore, Ryan Dix-Peek
title: Key Kotahi features
intro: A brief overview of some key Kotahi Features.
---


A selection of the top Kotahi features include:

### For the publishing team

1. **Flexible configuration**: enables publishers to customise the platform to meet their specific needs by adjusting workflows, user roles, metadata, and more, supporting a wide range of workflows from journals to the publish-review-curate (PRC) publishing model.
2. **Single source publishing**: allows publishers to create content once and publish it across multiple platforms and channels, such as web, XML, mobile, social media, and print.
3. **Automated typesetting**: enables publishers to format and layout text and images for print or digital publication quickly and accurately.
4. **Collaborative editing**: enables multiple users to work on the same document simultaneously, making it easier to collaborate and streamline the editing process.
5. **Workflow management:** enables publishers to manage editorial workflows, review processes, and other publishing tasks in a streamlined and efficient manner.
6. **Content import/export**: enables publishers to import and export content from a wide range of formats, including XML, HTML, and JSON.
7. **Metadata management:** enables publishers to manage metadata associated with content, such as author information, keywords, and publication dates.
8. **Multilingual support**: enables publishers to create and publish content in multiple languages.
9. **Analytics and reporting**: enables publishers to track user behaviour, content performance, and other essential metrics.
10. **AI Assist**: provides AI-powered assistance in various publishing tasks, such as metadata tagging, formatting, and content classification.
11. **Multitenancy:** allows a single instance of the platform to serve multiple publishers or organisations. This reduces costs and simplifies maintenance by avoiding the need for multiple platform instances. It also provides greater flexibility in managing and sharing content across different publishers and organisations.

### For developers

1. **Modular architecture**: breaks down the platform into smaller, more manageable components, making it easier to modify and extend.
2. **Plugin architecture**: enables developers to extend the platform with additional functionality, such as new import/export formats, editorial workflows, or integrations with other systems.
3. **Integration capabilities**: enables connections with other systems, such as content management systems, distribution platforms, and more.
4. **Open source**: means that the source code is freely available for viewing, modification, and redistribution.
5. **Community support**: provides valuable support, resources, and expertise and ensures that the platform remains up-to-date and relevant to the needs of publishers.
6. **AI subsystem**: provides a flexible and scalable AI infrastructure that developers can use to integrate AI capabilities into the platform, such as natural language processing, machine learning, and computer vision.
7. **Docker deployment support:** enables developers to quickly deploy the platform and its components in various environments using Docker containers, providing greater flexibility, scalability and efficiency in managing the platform's infrastructure.