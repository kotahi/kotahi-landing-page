---
layout: newsSingle.njk
tags: news
date: 2023-04-11T09:32:08.914Z
author: Adam Hyde, Paul Shannon, John Chodacki
title: An Initial Scholarly AI Taxonomy
intro: An article exploring an initial taxonomy for understanding Ai in
  publishing workflows.
picture: /static/images/uploads/adaminski_a_cafe_with_robots_and_quirky_hipsters_with_handlebar_94732657-f494-4e33-ae4f-40b62ed94e56.png
---
An article exploring an initial taxonomy for understanding Ai in publishing workflows. Publishing on the scholarly communications commentary site Upstream and authored by Adam Hyde, John Chodacki, and Paul Shannon.

<https://upstream.force11.org/an-initial-scholarly-ai-taxonomy/>