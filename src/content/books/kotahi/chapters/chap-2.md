---
title: "Introduction"
class: "component-body  chapter  "
order: 2
---

For years, academic publishing has relied on legacy systems that hinder efficiency and scholarly communication. Complex interfaces frustrate users. Rigid, one-size-fits-all submission systems cannot accommodate diverse content, workflows, or journal requirements. Email-driven communications cause delays. Production remains siloed externally without automation. Monolithic architectures hamper scalability.

At Coko, we believe it’s time to move forward. Driven by a passion for improving research sharing, we developed Kotahi to tackle these systemic challenges. Initially funded through our surplus, and later with help from partners like eLife who share our vision, Kotahi is designed to modernise workflows for traditional and emerging publishing models.

This documentation represents a key milestone for the project - the [Kotahi 2.0](https://kotahi.community/releases/) manual.

The platform tackles key challenges - making standards like JATS attainable without technical staff. It provides enormous flexibility to customise workflows as needed. Sophisticated multitenancy enables the hosting of multiple journals, preprint review, and preprint servers within a single install. Automated PDF production enables consistent, high-quality typesetting at scale. The microservices architecture allows independent scaling and updating of components.

Now, you will have the information in your hands on how to leverage the powerful features of Kotahi.

Scholarly publishing is evolving rapidly, and Kotahi is designed to improve how research is currently shared while anticipating future needs. By improving existing workflows and anticipating emerging models, Kotahi represents the innovative future of research communication.