---
title: "Additional Resources"
class: "component-body  chapter  "
order: 48
---

**Kotahi**  
[https://kotahi.community](https://kotahi.community/)

**Coko**  
[https://coko.foundation](https://coko.foundation/)

**Kotahi Support**  
[https://mattermost.coko.foundation](https://mattermost.coko.foundation/)

**Kotahi Code**  
[https://gitlab.coko.foundation/kotahi/kotahi](https://gitlab.coko.foundation/kotahi/kotahi)

**Wax**  
[https://waxjs.net](https://waxjs.net/)

**XSweet**  
[https://xsweet.org](https://xsweet.org/)

**Paged.js**  
[https://pagedjs.org](https://pagedjs.org/)

**Contact**  
Adam Hyde (Coko Founder): [adam@coko.foundation](mailto:adam@coko.foundation)