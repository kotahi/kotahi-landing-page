---
title: "What Use Cases Does Kotahi Support?"
class: "component-body  chapter  "
order: 6
---

Kotahi provides versatile tools extending far beyond traditional journal workflows. Kotahi enables end-to-end management of journal article submission through publication, with customisable reviewer selection, multi-round review, author proofing, and output generation.

However, Kotahi is not limited to mimicking journals. It can be used as a preprint server, house preprint review processes, provide overlay features, and innovative community curate and peer review models.

Additionally, Kotahi facilitates new output types like micropublications and interactive data papers. Authors can publish bite-sized findings enriched with multimedia and data visualisations.

While purpose-built for scholarly communication, Kotahi adapts for uses beyond academic research. Customisable stages, reviewers, and decisions enable grant proposal evaluation or student work submission and review.

In essence, Kotahi provides open architecture to empower new communication paradigms across research and education. It brings flexible workflows and formats beyond the limitations of traditional journals.