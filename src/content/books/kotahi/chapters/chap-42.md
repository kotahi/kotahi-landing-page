---
title: "Implementing Kotahi in a Lean, Iterative Fashion"
class: "component-body  chapter  "
order: 42
---

Kotahi provides extensive versatility to model diverse workflows through its configurable roles, forms, tasks, and review systems. However, fully customizing a complex platform can seem daunting. Here is guidance on rolling out Kotahi incrementally by blending workflow design concepts with lean development principles:

Start by Focusing on One Usecase
--------------------------------

Don't try to "boil the ocean" upfront. Focus your initial implementation on the primary or easiest use case. Get one core workflow fully operational before expanding scope.

Focus on Workflow First
-----------------------

Start by modeling your first optimal use case (as above) and workflow, independent of technology constraints. Use techniques like workflow mapping and sprints. Define key roles, content flows, and bottlenecks to address.

Deploy in Small Batches
-----------------------

Map the conceptual workflow to Kotahi in stages, starting with a minimal viable product. For example, first focus on getting base submission and review forms operational.

Gather User Feedback
--------------------

After each incremental deployment, solicit user feedback through direct discussion. Identify pain points and opportunities.

Continuously Improve
--------------------

Utilize feedback to rapidly iterate improvements aligned with the workflow vision. Add features, enhance forms, modify configurations etc. in small batches.

Foster Collaboration
--------------------

Facilitate open communication with your users throughout the process. If possible, form a cross-functional team empowered to collaboratively guide the rollout.

By blending workflow design with lean, collaborative development practices, Kotahi can be implemented iteratively at a sustainable pace. The focus is on incrementally enhancing real-world operations through participatory prototyping and user-centered improvement. With this adaptive approach, the full benefits of Kotahi’s versatility can be realized over time.