---
title: "What Review Models are Available?"
class: "component-body  chapter  "
order: 8
---

Kotahi supports a wide range of peer review models that can be customised for your journal's needs. Options include:

*   **single-blind review** - reviewers know the author's identity but their identity is hidden from the author
    
*   **double-blind review** - the identities of both the author and reviewers are hidden from each other
    
*   **open review** - the identities of the authors and reviewers are known to each other
    
*   **collaborative review** - reviewers can discuss the submission together and collaborate on a review. This can be moderated or unmoderated.
    
*   **community self-review** - the review process allows the community to self-select as reviewers without editor assignments. Often used for preprint reviews.
    
*   **multiple rounds** - as many iterative rounds of review as needed with the same or new reviewers each round (typical for Journals)
    
*   **rolling submissions** - authors can update submissions during an active review round if enabled (this has proven useful for some preprint review use cases)
    

In summary, Kotahi is highly configurable to support single-blind, double-blind, open, collaborative, community self-review, and combinations of these models. The system allows multiple iterative review rounds with flexible reviewer selection and updated submissions.