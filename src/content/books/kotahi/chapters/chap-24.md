---
title: "Language Selection"
class: "component-body  chapter  "
order: 24
---

Select your preferred language from your Profile page.

All interface component language strings are accessible for translation via the backend. You can add a new language or alter a string to personalise your experience e.g. changing the role of ‘Editor’ to ‘Curator’ in all cases where the text appears.

![](/images/b9e051788740_medium.png)