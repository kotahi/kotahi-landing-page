---
title: "How to use this manual"
class: "component-body  chapter  "
order: 1
---

This manual covers the use of Kotahi. While it provides Kotahi documentation, its scope is broader - to help you rethink and design workflows.

This manual aims to:

*   explain Kotahi's capabilities
    
*   provide tools to evaluate, design, and evolve workflows.
    
*   guide Kotahi configuration to encapsulate your way of working
    
*   provide assistance to all Kotahi users
    

If you first wish to understand how Kotahi works, we have included descriptions of all the interfaces and high-level concepts in the chapters _Kotahi Explained, Settings, CMS_, and _Production_.

If you wish to get quick help on specific topics, go to the _How To Guides_ section.

However, the workflow chapters provide important conceptual foundations. _The Tips on Setting Up Your Workflow_ section especially bridges workflow design and Kotahi capabilities.

Consider reading the workflow chapters to reflect on processes, even if you are not changing your workflow now. Technology decisions should follow, not drive, workflow needs. Use this manual to take a step back and rethink how work gets done.