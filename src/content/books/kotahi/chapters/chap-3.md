---
title: "In a Nutshell"
class: "component-body  chapter  "
order: 3
---

### What is Kotahi?

Kotahi is an open source publishing platform designed to modernise workflows for journals, preprint servers, preprint review communities, and other models. It provides a flexible system to support diverse publishing needs.

### What are the key features of Kotahi?

Some key features include configurable workflows, multitenancy, automated JATS and PDF production, drag-and-drop form building, support for any metadata schema, tailored peer review, real-time communications, task management, and more.

### What publishing models can Kotahi support?

Kotahi can support traditional journals, preprint servers, publish-review-curate models, overlay journals, micropublications, and more. Its flexibility enables many emerging publishing paradigms.

### How does Kotahi support preprint review?

Kotahi enables the import of preprints, AI-powered recommendations, custom review forms, collaborative annotation, grouped reviews, and flexible publishing of reviews or curated collections.

### What review models does Kotahi offer?

Kotahi supports single-blind, double-blind, open, collaborative, community self-review, multiple iterative rounds, and combinations of these models.

### How does the Kotahi form builder work?

The intuitive drag-and-drop form builder allows creation of submission, reviewer, and decision forms without coding. Forms can be designed from templates or customised.

### How does Kotahi produce JATS XML?

Kotahi provides a simple production editor for users to visually tag document sections. These are automatically mapped to JATS XML elements on export.

### How does Kotahi create PDFs?

Kotahi leverages Paged.js to automatically paginate manuscripts into print-perfect PDFs with professional typesetting and formatting.

### How does task management work in Kotahi?

Advanced but customisable task management allows configuring workflows at a per-group or per-manuscript level with reminders, actions, and invites.

### What multitenancy capabilities does Kotahi offer?

Kotahi enables hosting multiple isolated journals, preprint servers, or review groups within one installation while retaining custom workflows for each.

### How is Kotahi designed to be customisable?

Kotahi is highly configurable through its settings and also extensible via plugins, APIs, and microservices. Coko offers customisation services.

### What benefits does Kotahi's architecture provide?

The microservices architecture enables independent scaling and updating of components along with overall flexibility and resilience.

### How difficult is it to install Kotahi?

Kotahi utilises Docker and microservices to simplify deployment. While sysadmin skills help, the goal is accessible self-service installation. There are third-party hosting and publishing services vendors that can also help you with Kotahi hosting.

### Is Kotahi open source?

Yes, Kotahi is 100% open source software published under the MIT license. The code is freely available on GitLab.

### How do I get technical support for Kotahi?

Join the Coko Mattermost for community support at [https://mattermost.coko.foundation](https://mattermost.coko.foundation/)

### Who created Kotahi?

Kotahi was created by Coko, a non-profit developing open source publishing infrastructure. Learn more at [https://coko.foundation](https://coko.foundation/)