---
title: "What Can Kotahi Publish?"
class: "component-body  chapter  "
order: 7
---

Kotahi is highly configurable and can publish a wide variety of research outputs. Here are some of the main options:

Types of content
----------------

*   **Evaluations** - Review content and decision summaries from the peer review process. This could include full verbatim reviews or edited excerpts.
    
*   **Data** - Datasets, metadata, code, multimedia, and other supplementary files.
    
*   **Manuscripts** - Preprints, journal articles, conference papers, micropublications, and other scholarly documents.
    

![](/images/73adabf61ed7_medium.png)

Publishing destinations
-----------------------

*   **Internal CMS** - Kotahi has a built-in content management system (CMS) to display content. The CMS is customisable to fit your needs.
    
*   **External endpoints** - Kotahi can publish to any external endpoint or system provided it can receive the data. This includes institutional repositories, preprint servers, journals, archives, social media, and more.
    
*   **Multiple targets** - Content can be published to the internal CMS, external endpoints, or both simultaneously.
    

Configuration Options
---------------------

Kotahi allows flexible configuration of publishing in various ways:

*   publish any combination of evaluations, data, and manuscripts or none at all
    
*   publish to single or multiple endpoints
    
*   customise metadata, identifiers, licenses, formatting, layouts, and more
    

In summary, Kotahi enables publishing virtually any scholarly content anywhere through a highly configurable system.