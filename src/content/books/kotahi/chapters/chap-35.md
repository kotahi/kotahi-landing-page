---
title: "Making a Manual Submission"
class: "component-body  chapter  "
order: 35
---

Both Manual and Automated Submissions can be made in Kotahi. This chapter covers manual submissions, please also see the appropriate section on _Automated Submissions_.

Manual Submissions are made from your Dashboard.

![](/images/4e20bd146ed4_medium.png)

When you click the ‘+ New Submission’ button you will be presented with a new screen:

![](/images/8f178aba97d4_medium.png)

There are two basic methods to create a submission:

1.  **Upload Manuscript** - by clicking ‘+Upload Manuscript’ you can choose a research object to upload. The types of filetype accepted depends on the configuration of your group.
    
2.  **Submit a URL instead** - by clicking on this you can simply add a URL as a submission (good for preprint reviews or submitting dynamic notebooks, code repositories, online data sets etc)
    

Upload manuscript
-----------------

There are two major categories for Upload Manuscript:

1.  **Selecting a docx** - in this case, the MS Word file is ingested into Kotahi and can be viewed and edited in the manuscript editor.
    
2.  **Selecting another filetype** - in this case, the manuscript is treated as an attachment.
    

Entering submission data
------------------------

After you have uploaded a manuscript (or provided a link) you will be automatically redirected to the Submission page:

![](/images/db3349b274b5_medium.png)

Note, the page displayed in this image is an example and may differ from how your Submission page looks.

You can now enter data to fill out the submission form. It is also possible to toggle between the manuscript and the submission data by clicking on the tabs ‘**Edit Submission info**’ and ‘**Manuscript text**’).

When you have entered all the data press ‘**Submit your research object**’. You may be prompted to complete a Terms & Conditions/disclaimer before submitting.