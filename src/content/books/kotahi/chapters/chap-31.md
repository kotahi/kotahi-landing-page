---
title: "Using the Automated Citation Tools"
class: "component-body  chapter  "
order: 31
---

When using the production interface, the team can check the citations and improve them with the assistance of Kotahi’s automated citation tools.

To access these tools, view the research object in the production interface (linked from the Manuscripts page) and scroll down to the reference list.

![](/images/ea804cdf4a84_medium.png)

On the left, you will see a reference tool:

![](/images/69aa1bd4a869_medium.png)

Select an item in the references and then click on this reference tool. You will see a menu appear to the right of the reference.

![](/images/c683437844d1_medium.png)

Clicking on one of these icons will display an overlay.

![](/images/d66afa263b88_medium.png)

Here you see a list of possible citations based on the information in the original citation. The recommended citations come from a variety of sources (more can be added). Native to Kotahi 2.0 are AnyStyle and Crossref sources.

You can now choose one of the citations displayed (these also conform to the citation format set in the Kotahi system settings).

You can also select one of these citations and click ‘edit’ and a further box will display for editing that selection.

![](/images/c1abe6a90ba8_medium.png)

This service relies on 3rd party APIs to generate results. These services may be disrupted from time to time. You may need to re-run imports if this service does not generate results as expected.

Settings to change the citation style and other related controls are located in the Configuration→Production section.

![](/images/0f92460cbb2e_medium.png)