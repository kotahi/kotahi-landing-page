---
title: "Kotahi Setup Checklist"
class: "component-body  chapter  "
order: 43
---

There are essentially two phases for setting up Kotahi. The first involves planning, the second is implementation.

Planning
--------

☐ Understand what type of object you wish to process and publish and document it’s attributes (what kind of data).

☐ Plan your workflows - Map out your team's current or ideal workflows using workflow notation.

☐ Document your Submission, Decision, and Review forms.

☐ Understand the structure of your publishing end point(s), what you are sharing and where.

☐ Gather or create your branding materials (colors, logos etc)

Implementation
--------------

☐ Install Kotahi - Get a Kotahi instance running yourself or pay a service provider to do it.

☐ Brand Kotahi - Customize the look, feel from the System Settings.

☐ Configure system settings - Set up core parameters like notifications, crossref integrations etc.

☐ Build submission forms - Construct customized forms to capture metadata from authors.

☐ Design review forms - Create tailored forms to collect feedback from reviewers.

☐ Model tasks - Configure task templates to define workflow steps.

☐ Customise PDF - Configure PDF to your inhouse designed (if required).

☐ Customise CMS - Customise the Kotahi CMS or integrate with an external CMS.

☐ Invite intital users - Onboard team members by sending invitation emails to create accounts.

☐ Run tests - Validate workflows, forms, and integrations function as intended.

☐ Go live - Flip the switch to activate for real-world usage!

☐ Gather feedback - Continuously collect user feedback to drive improvements.

☐ Iterate and optimize - Use feedback to rapidly iterate and enhance workflows over time.