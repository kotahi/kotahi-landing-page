---
title: "Technology Supporting People"
class: "component-body  chapter  "
order: 38
---

Kotahi's core design principle is that technology should support people and processes, not that people should serve technology. The platform aims to provide adaptable infrastructure for configuring publishing operations while giving users control to optimise over time.

This means Kotahi supports both streamlining established operations and experimenting with innovative models. Teams can use Kotahi to make existing procedures more efficient and pilot progressive ideas in parallel.

Easy reconfiguration of components like tasks, forms, and decision screens makes iterating processes seamless. Teams can continuously adapt approaches based on data and feedback.

With sandboxed multitenancy, Kotahi encourages trying new workflows with minimal risks. Fresh concepts can be tested without disrupting current operations.

At its core, Kotahi recognises publishing processes should be led by your needs not ‘how technology works’.