---
title: "Producing JATS"
class: "component-body  chapter  "
order: 32
---

JATS is the Journal Article Tagging Suite. It is an XML format used to describe a journal article.

Creating XML is usually done via inhouse technical staff or outsourced to a publishing vendor. This generally increases the costs for processing articles and slows down the time to publish.

Kotahi has inbuilt tools that enable you to create high fidelity JATS output at the press of a button and with no technical expertise.

The production editor includes features related to creating JATS.

![](/images/f359f5ce2689_medium.png)

The user requires no prior knowledge of JATS or XML to prepare the document for JATS export. The interface simply requires the user to highlight parts of the document and choose what type of content that selection contains from the tools in the left menu. This is very much a drag-and-drop and point-and-click exercise. No messy editing of XML required.

The Production editor then marks the underlying format (HTML) in such a way that Kotahi can map these tags to JATS at export time.

The following annotations can be applied in the Production editor;

*   **Front matter** - Abstract, Funding group, Funding source, Award ID, Funding statement, Keyword list and Keyword.
    

*   **Back matter** - Appendix, Acknowledgements, Glossary Section, Glossary term, Reference list and Reference (parser).
    

![](/images/93053e83a26c_medium.png)