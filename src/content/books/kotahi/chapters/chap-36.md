---
title: "Reviewing"
class: "component-body  chapter  "
order: 36
---

If you have been invited to review, you will have received an email.

![](/images/8b4c7d40cd82_medium.png)

The actual text of your email may differ from the above. The email invitation includes a link to accept the invitation and a link to reject the invitation.

If you choose to accept, then you will be asked to login and redirected to your Dashboard. In the ‘To Review’ tab you will see your item for review:

![](/images/7444c03c0fc4_medium.png)

Clicking **Do Review** on the right will take you to the reviewing page for the item:

The page will differ depending on how your version of Kotahi has been configured. You will see the following pages:

*   **Metadata** - displays all submission form data shared with reviewers.
    
*   **Manuscript** - displays content that has been converted from a Word document submission
    
*   **Review** - your (reviewers) review form.
    
*   **Other Reviews** - any submitted reviews that have been ‘Shared’ by the editor.
    

Scrolling down will display all the relevant data.

![](/images/04034ce89aed_medium.png)

Again, the review form is entirely configurable so you will probably see something different to what is displayed in this image.

You may now complete the review form and submit your review. If you have not completed the review, you may come back to it at a later date and the content of your partial form will persist across sessions.

Depending on how Kotahi has been configured for you, you may be able to upload a PDF or other file that contains your review.