---
title: "Roles"
class: "component-body  chapter  "
order: 14
---

The basic Kotahi roles are as follows:

Admin
-----

Admin is a global role with access to all groups. An Admin has system administration permissions and access to the Dashboard and Settings pages. Admin permissions support the configuring of groups (Configuration manager, Forms, Task templates, Email templates and CMS settings) and managing Users globally. Admins don't have permission for various editing tasks unless they are assigned as Group Managers or Editors.

Group Manager
-------------

A Group Manager has managerial permissions within a group. They can access the Dashboard, Manuscripts, Reports and Settings pages. A Group Manager can assign editors. They can also perform editorial tasks themselves even without being assigned as an editor. They also can manage users within their group.

A Group Manager is the only role that has access to the Manuscripts page and has oversight of all the manuscripts within their group. This role can facilitate a curation role in support of manuscript triage, for example.

Currently, this role also has access to the Production editor from the Manuscripts page.

Admin users who are also assigned as Group Managers can delete any Discussion message in a given group.

Editor
------

Has permissions needed to access the Control page for managing the research objects they are assigned to.

This includes assigning other team members, inviting reviewers, editable access to metadata and decision/evaluation data as well as the ability to edit task lists and communicate with all stakeholders.

All research objects assigned are listed on the Dashboard→Manuscripts ‘I’m Editor of’ tab.

Reviewer
--------

Has permissions needed to review the research objects they are assigned to. This includes access to the Review page and review form, and the ability to communicate with the editorial team.

All reviews assigned are listed on the Dashboard→To Review tab.

Author
------

Can access and manage the submissions they have created or are associated with.

Authors can submit multiple versions of research objects and/or datasets and communicate with the editorial team.

All submissions are listed on the Dashboard→My Submissions tab.