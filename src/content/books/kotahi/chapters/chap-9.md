---
title: "Pre-configured Workflows"
class: "component-body  chapter  "
order: 9
---

Kotahi supports several pre-baked workflows for a quick start without having to understand all the congfiguration possibilities beforehand.

The four pre-configured workflows (it is possible to add more with the assistance of a developer) include:

Journal publishing workflow
---------------------------

Supports an end-to-end traditional journal publishing workflow; author submits, editors triage, with support for multiple peer review cycles and publishing to Crossref and other endpoints including export to PDF and JATS. 

![](/images/e237f28570c3_medium.jpeg)

Publish, Review, Curate
-----------------------

Supports an end-to-end publish, review and curate (PRC) workflow; imports manuscripts from a preprint server, editor triage, customisable submission, review and evaluation forms and publishing endpoints. 

![](/images/bf8f80ebbcd9_medium.jpeg)

Preprint review
---------------

Use a single submission form to capture and publish metadata, peer reviews and/or evaluation reports. The right choice if the team is not required to manage an end-to-end workflow, and a lightweight solution that’s easy to use. 

![](/images/d65905d0045c_medium.jpeg)

Preprint review 2
-----------------

Import manuscripts from a preprint server. Use a single submission form to capture and publish metadata, peer reviews and/or evaluation reports to an endpoint. This configuration makes use of a Manuscripts table, providing an overview of all objects in the system, with specific features to support the triage process. 

The right choice if the team is not required to manage an end-to-end workflow, and what’s needed is a lightweight solution that’s easy to use. 

![](/images/cd23930e8b9b_medium.jpeg)