---
title: "Managing Users"
class: "component-body  chapter  "
order: 21
---

Users are managed from Settings→Users.

![](/images/410443f5a9f4_medium.png)

Here you see a list of all users in the system. The users are listed with the following information in columns:

*   **Name** - the full name of the user (with a picture if provided by the user in their profile page)
    
*   **Created** - the date the user created an account
    
*   **Last Online** - when the user was last online
    
*   **Roles** - the role(s) they’re assigned
    
*   **Delete** - deletes the user from the system (requires a confirmation)
    

All columns are sortable by clicking the column title.

Roles
-----

There are two roles you can set at this interface that have to do with the administration of the system - Group Manager and Admin.

*   **User** - a user assigned either as an author, reviewer and/or editor in a group
    
*   **Group Manager** - has permission to access all group pages listed in the menu, essentially a ‘Group Admin’ role
    
*   **Admin** - has access to all groups on an instance and has limited access to group pages