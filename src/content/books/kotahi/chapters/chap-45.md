---
title: "General Technical Information"
class: "component-body  chapter  "
order: 45
---

Kotahi is an open source publishing platform built using modern JavaScript technologies.

Code, issues, and project management
------------------------------------

*   Codebase: [https://gitlab.coko.foundation/kotahi/kotahi](https://gitlab.coko.foundation/kotahi/kotahi)
    
*   Issues: [https://gitlab.coko.foundation/kotahi/kotahi/-/issues](https://gitlab.coko.foundation/kotahi/kotahi/-/issues)
    
*   Roadmap: [https://gitlab.coko.foundation/kotahi/kotahi/-/milestones](https://gitlab.coko.foundation/kotahi/kotahi/-/milestones)
    
*   Project Boards: [https://gitlab.coko.foundation/kotahi/kotahi/-/boards/317](https://gitlab.coko.foundation/kotahi/kotahi/-/boards/317)
    

Installation and documentation
------------------------------

*   Install guide: [https://gitlab.coko.foundation/kotahi/kotahi/-/blob/main/README.md](https://gitlab.coko.foundation/kotahi/kotahi/-/blob/main/README.md)
    
*   License: MIT
    
*   Docker-based deployment
    

Technology stack
----------------

*   Node.js
    
*   React
    
*   Express.js
    
*   GraphQL
    
*   Postgres database
    

Integrated microservices
------------------------

*   XSweet: Microsoft Word conversion
    
*   Anystyle: Citation parsing
    
*   Paged.js: PDF generation
    

Additional Coko Components
--------------------------

*   Wax editor: Used in forms, the CMS, the production editor, the manuscript editor
    
*   FLAX CMS: For public-facing pages
    

Support
-------

For technical support, join our Mattermost channel: [https://mattermost.coko.foundation](https://mattermost.coko.foundation/)