---
title: "Overview"
class: "component-body  chapter  "
order: 26
---

Kotahi offers configurable publication options, including an internal CMS. Content can be published to the CMS, external endpoints, or both.

The Kotahi CMS design follows the principle: ‘Make simple things simple, and complex things possible.’

The CMS interface enables straightforward management of basic websites and content publishing. This simplicity makes routine tasks easy.

However, the CMS also provides tremendous flexibility to customise sites. It can be structured to function as a journal publishing platform, a preprint server, a micropublication repository, a preprint review publisher, or virtually any model required. With some configuration, the Kotahi CMS can handle highly specialised and structured content. Its versatility supports diverse scholarly publishing needs.

The CMS balances simplicity for basic use cases with extensive customisation potential for complex requirements. Teams can start simply and then evolve the CMS as needs grow. This range encapsulates the design principle driving the system. The CMS aims to make simple things simple while enabling complexity when required.