---
title: "What is Publish-Review-Curate (PRC)?"
class: "component-body  chapter  "
order: 51
---

Publish- Review-Curate (PRC) represents an emerging model of research communication combining preprint posting, community peer review, and curation.

The stages of PRC are:

**Publish** - Researchers first publish preprints openly on servers like arXiv and bioRxiv for early sharing of findings.

**Review** - Volunteer communities then conduct peer review of preprints in a transparent, public process.

**Curate** - Preprints undergo a final curation stage for further validation, improved formatting, and communication of findings.

This sequence flips the traditional journal path of Review → Curate → Publish to:

Publish → Review → Curate

Some key aspects of PRC:

*   Accelerates research dissemination via preprints prior to formal peer review. Enables rapid sharing of findings.
    
*   Community peer review of preprints in a publicly visible manner amplifies transparency.
    
*   Curation stage improves preprint formatting, legibility of findings, and provides aggregation around topics.
    
*   Curators can be traditional journals but also communities and new forms of curatorial organisations.
    
*   Blends rapid sharing of preprints with improved communication after review/curation.
    
*   Combines benefits of preprints, community review, and curation.
    

PRC balances accelerating discoveries through preprints with the improved clarity and validation of findings in curated versions. It also maintains focus on preprints as the core, shared object flowing through the processes.

The PRC model represents an evolution of research communication - leveraging preprints to accelerate sharing while retaining the value-add of peer review and curation.

Importantly, PRC is not just a theoretical model but an approach already being adopted by scholarly communities across disciplines.