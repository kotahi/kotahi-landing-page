---
title: "Building a Reviewer Form"
class: "component-body  chapter  "
order: 18
---

You can build your own reviewer form through the form settings. Tailored reviewer forms focus the process, reduce fatigue, provide guidance, collect consistent structured data, and adapt over time. This ultimately leads to more robust, useful reviewer feedback and analytics.

To get started choose Settings→Forms→Review.

![](/images/9a3cfb6b7a58_medium.png)

The above image shows a completed reviewer form. Building a review form is essentially the same process as building a submission form (see documentation).

It is important to note that Kotahi enables individualised and shared reviews. These two options are not controlled here (see documentation on reviews).

### Support for Multiple Review Forms

Currently, you can create multiple review forms but only a single form can be selected for use. Only the **Form purpose identifier** field populated with ‘review’ will be available to reviewers.