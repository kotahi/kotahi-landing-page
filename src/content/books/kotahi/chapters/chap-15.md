---
title: "Tips On Setting Up Your Workflow"
class: "component-body  chapter  "
order: 15
---

The easiest way to get started with workflows in Kotahi is to use a preconfigured group template. But for fully custom workflows, mapping desired processes to Kotahi's configuration is key. Here are some tips.

Hierarchical vs flat community workflow
---------------------------------------

Kotahi allows configuring hierarchical roles or a flat community structure - it's flexible to fit different needs. By default, it separates Authors, Reviewers, and Editors into distinct roles. However, a flat hierarchy can be enabled in Settings → Configuration by checking ‘All users are assigned Group Manager and Admin roles’. It is also possible to manually make users Group Managers or Admins via Settings → Users which may assist if you want a ‘mixed’ model.

In flat communities where all participants can see everything, it also makes sense to set the Manuscripts page as the landing page upon login. This is configured in the ‘Landing page for Group Manager users’ dropdown.

With these simple tweaks, Kotahi can be adapted from hierarchical journal workflows to flat, open review communities. The roles and permissions model is customisable to enable varying levels of openness and transparency. You can craft optimal settings for your reviewers and authors to collaborate.

Review model
------------

A key component in configuring the review model is customising the Review Form under Settings → Forms → Review.

This flexible form builder allows the creation of any desired structure for reviews, including custom rating systems. For example, you can add a ‘Select’ form element and use HTML star codes to easily build a 5-star rating selector. The possibilities are extensive - add text sections for comments, numeric scales for ratings, multiple choice questions, and more. Granular customisation of the Review Form enables tailoring the process to fit your evaluation needs. And helpful features like metric selectors and star ratings make it simple to collect key quantitative feedback. With full control over the form, you can craft an optimal review experience for your community.

![](/images/c67551db8452_medium.png)

Building a 3-star rating element using the Review form builder

Further, you can set up various kinds of review process.

### Single blind

When authors are visible to the reviewer, but the reviewer is not visible then this is managed from the Control page for each research object by selecting ‘Hide reviewer name’.

![](/images/d4e89f695540_medium.png)

If ‘Hide reviewer name’ is selected, the reviewer's identity is anonymised;

*   when submitting (sharing) Decision/Evaluation form data with the author
    
*   on the Review page if a review is set to ‘Shared’ with other reviewers
    
*   on the publish action when publishing to Flax
    
    ![](/images/9bc7058894a0_medium.png)
    
    Anonymous review on the Review page
    

### Double blind

In the case of a double-blind review where both the authors names and the reviewers name are hidden from each other, use the setting above to hide the reviewer name.

Second, selectively hide author names from reviewers. Under Settings → Forms → Submission, any form components that display author details like names, affiliations or the MansucriptFile, should have 'Hide from Reviewers?' selected.

![](/images/777185b653f3_medium.png)

Lastly, from the manuscript or production editor, you will need to delete the author names from the manuscript.

This obscures manuscript-identifying information from reviewers to complete the double-blinding. Between hiding reviewer and author details, you can easily implement fully anonymous peer review.

### **Open review**

The identities of the authors and reviewers are known to each other. In which case, don’t hide the authors fields from reviewers in the submission form, leave the author names in the manuscript, and share the reviewers name from the Control page settings.

### Collaborative review

Reviewers can discuss the submission together and collaborate on a review. This can be moderated or unmoderated and is managed at the moment you invite a reviewer by selecting ‘Shared’.

Only reviewers with ‘Shared’ enabled can see each other’s reviews on the Review page.

![](/images/74fa68650396_medium.png)

The option to enable collaborative reviewing is kept as a manuscript-level setting rather than global, to allow flexibility in mixing collaborative and independent reviews. For example, you may want 2 reviewers to collaborate on one manuscript while 2 others write independent reviews on the same submission.

This granular control makes it possible to have a hybrid approach within the same journal or collection. You can choose collaborative reviews when you want reviewers to discuss and consolidate feedback, while independent reviews give you multiple perspectives.

The per-manuscript configuration provides the most flexibility to tailor peer review based on the needs of each submission. You can determine the optimal individual vs collaborative review structure case-by-case.

### Community self-review

For peer review models where the community self-selects to review manuscripts, enable access to submissions under Settings → Configuration and check ‘All users are assigned Group Manager and Admin roles’. This will make the Manuscripts page visible to all users, allowing community members to freely choose papers to review.

This facilitates review without editor assignments. Reviewers can simply pick submissions of interest to them and self-organise review efforts through the associated discussion.

The chat on the Manuscripts page provides a space for transparently coordinating who will review each item. Community members can claim papers, avoid duplication of efforts, and fill gaps.

With accessible manuscripts and integrated coordination tools, Kotahi can effectively support open self-selected peer review. The platform enables the community to democratically manage the process from paper discovery to discussion.

### Multiple rounds

You can have as many iterative rounds of review as needed with the same or new reviewers each round (typical for Journals). A new round is started by submitting a decision to revise from the Control page.

### Rolling submissions

Authors can update submissions during an active review round if enabled (this has proven useful for some preprint review use cases). You can enable this by checking Settings → Configuration → Allow an author to submit a new version of their manuscript at any time.

Workflow status
---------------

The submission form builder can be used to create tools to manually update and monitor the status of research objects as they are being processed. To do this, you can use the ‘Select’ form element (see above) to build a drop down menu outlining the processing stages. You can have any number of states, and additionally, the form builder enables you to choose a colour per state should you so wish so you can easily understand state at a glance.

This status display can be included in the Manuscripts page as a colum if you so wish by adding the internal name to the comma-separated list in Settings → Configuration:

![](/images/9f3ef13af8bc_medium.png)

Kotahi also has automated generic states that can also be included in the Manuscripts page by adding ‘status’ to the same list.

It is also possible to display the state tracker to the Control page by adding it to the decision form in a similar manner.

Taxonomy / Groups
-----------------

Kotahi allows creating customisable taxonomies or groups for research objects using the ‘Select’ form component. These taxonomies are editable, so new types can be added as needed.

Including the taxonomy dropdown in the Manuscripts page columns provides filtering and grouping capabilities. Users can select a specific type from the dropdown to view only those matching objects. The filtered list URL can also be shared for collaboration.

For example, a group could contain types like ‘Type A’ and ‘Type B’. Filtering to ‘Type B’ would show all research objects in that group. The URL could be shared with others to coordinate efforts.

This enables segmenting objects in flexible ways - by status, priority, subject area, etc. Taxonomies help organise community work by creating specialist lists. And the ability to amend types supports iterative workflow design.

Deciding what to publish
------------------------

There are many controls and settings in Kotahi to decide what you will publish. If you wish to publish data, for example, you may not need to supply a manuscript upon submission. In this case, you can hide the page displaying the manuscript from view from Settings → Configuration

![](/images/9e1818ba9467_medium.png)

You can also decide what data will be published when setting up the submission form. Each element has the option as follows:

![](/images/e0e70ef605a4_medium.png)

### Publishing evaluations / reviews

The review and decision forms in Kotahi can be fully customised using the form builders. This enables granular configuration of what data is captured in the review and decision processes.

Using the form builders, you can:

*   choose which review and decision fields are shared or published, and which remain internal
    
*   decide if full verbatim reviews are published or if curated excerpts are preferable
    
*   determine if overall decision summaries, reviewer recommendations, or any other custom fields are disseminated
    
*   select specific metrics like ratings or scores for publication rather than full text
    
*   opt to only share the decision outcomes such as ‘accept’ or ‘reject’ without additional details
    
*   hide reviewer identities from the published decision data as needed
    

The review and decision form builders allow optimising the data workflow - controlling what reviewers provide, limiting internal-only data, and configuring the published evaluation content. This enables sharing evaluations in customised formats, from high-level decisions to granular, transparent assessments. The system accommodates community preferences in determining what aspects of the process to openly disseminate.

### Formating

Kotahi offers extensive formatting flexibility when publishing content internally to its built-in CMS. The research object can be laid out and styled in any desired way, including custom components like data visualisations.

The internal CMS enables complete control over publication formatting. Content can be structured with sections, styled with CSS, and enriched with multimedia embeds as needed. Articles, data, code, images, and other supplementary materials can be tightly integrated.

For example, a published preprint review could contain the full community discussion threaded directly alongside curated recommendations. Interactive graphics could also enhance understanding of the analysis.

While publishing externally limits format control, the internal CMS provides limitless customisation possibilities. Creative publishing styles can truly amplify the value of the research object.

Tasks
-----

The Kotahi task manager is a powerful workflow design and management tool worth investing time into. Tasks allow granular mapping out process steps, ordering, notifications, and roles.

Rather than just tracking tasks, the manager can be leveraged to actively conceptualise ideal workflows. The customisable main task template essentially serves as a blueprint for iterative workflow design.

As workflows evolve, the template can be modified to reflect new processes. Additional ad hoc tasks can also be added to individual objects as needed. This balance of structure and flexibility enables both workflow standardisation and handling unique cases.

Key benefits of the task manager:

*   visualise and conceptualise workflows through customisable templates
    
*   promote consistency via predefined task sequences
    
*   enable flexibility for per-object adjustments as needed
    
*   set notifications to coordinate community members
    
*   refine tasks continually as workflows improve
    
*   combine global templates and per-object adjustments
    

With powerful abilities to model workflows plus adapt them over time, the task manager is a valuable design asset. Investing in its configuration pays dividends for conceptualising, communicating, and continually improving publishing processes.

There is much more to designing and setting up your workflow. Kotahi enables you do do so much that most of what is possible hasn’t even been considered yet! The best thing is to experiment.