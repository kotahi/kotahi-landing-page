---
title: "The Coko Manifesto"
class: "component-body  chapter  "
order: 47
---

Publishing is stuck. Legacy organisations are wedded to outdated models, employing rigid processes on antiquated software. This yields expensive, sluggish, and wasteful systems that are stunting better ways of sharing knowledge. Dominant structures and inflexible conventions have stalled attempts to improve publishing, as core power centres remain entrenched and change stays superficial.

But while the current implementation is broken, there is value in what publishing is trying to achieve - to create, refine, and share knowledge. We must reconnect to this core purpose and revisit how we achieve it.

The path forward starts with re-envisioning publishing from its foundations.

At Coko, our focus is on developing the fundamental building blocks enabling publishing to be reimagined in this way. We are constructing core open infrastructure, tools and platforms - aligned with publishing's true purpose of advancing collective knowledge. With this open infrastructure in place, pioneering new publishing models become possible.

From this position of collective power, we envision a boundless future for publishing, where bold new models emerge through open collaboration. A future transformed by radical openness, where minds connect across organisations to co-create, innovate, and progress together.

In this open ecosystem, cross-functional teams seamlessly collaborate—powered by needs and human-centric technologies, not legacy conventions and obsolete systems. The boundaries between creators and publishers dissolve, as they merge together in the service of knowledge creation—not antiquated organisational structures.

Through open collaboration, knowledge becomes more accessible, more representative, more meaningful. Timelines accelerate, costs plummet, and errors disappear as capabilities expand continuously.

Guided by our core values of trust, openness and collective empowerment, we must reimagine publishing from the ground up together. Our shared responsibility is to pioneer these new workflows, technologies, and models that unlock publishing's immense collaborative potential.

Not simply improving publishing, but redefining it.

This is publishing's collaborative future. This is Coko’s journey.

**We advocate for the adoption of open web formats throughout the publishing workflow.** Open web formats (eg, HTML, JSON, CSV) should be embraced as core publishing formats. They enable publishing on the world's largest collaborative platform - the web.

*   Enable multi-modal content
    
*   Enable real-time collaboration
    
*   Proliferation of format conversion options
    
*   Support progressive structuring
    
*   Enable single source publishing
    

**We advocate for Single Source systems.** By using one content source across authoring and production, Single Source systems enable seamless collaboration, reduce handoffs, eliminate conversions, condense timelines, and connect workflows.

*   Use open web formats throughout the process
    
*   Connect content creation and production
    
*   Enable concurrent workflows
    
*   Allows real-time collaboration
    
*   Reduce handoffs between teams
    
*   Lower costs by eliminating conversions
    
*   Condense overall timelines
    
*   Promote synchronous operations
    
*   Reduce errors by minimising versions
    
*   Adapt to future needs
    

**Publishing has become unnecessarily complex.** The current fragmented systems result in disconnected teams, wasted effort, and preventable errors.

**Publishing is a process, not an organisation.** Publishing should focus on needs, not be constrained by existing organisational structures.

**The boundaries between authors and publishers must dissolve.** We must move past artificially separated systems and embrace collaborative creation and dissemination.

**New, open processes require new, Open Source technologies.** To reimagine publishing, innovative workflows require empowering new technologies.

**Collaboration, concurrency, and communication are essential.** By integrating real-time teamwork into systems, we can break down silos, work in parallel, condense timelines, and reduce costs.

**Deep collaboration changes everything**. Deepening collaboration transforms structures, flattening hierarchies, decentralising leadership, and shifting to collective ownership and integrated teamwork.

**Publishers must embrace facilitative leadership.** Facilitation unlocks collective potential as dominance gives way to inclusion, silos are replaced by openness, and individual agendas dissolve into shared purpose. The end result is empowered teams, accelerated workflows, and publishing that truly serves its community.

**Systems should be designed with open, cross-functional spaces.** Cross- functional digital spaces allow teams to collaborate without boundaries. Hardcoded workflows construct gated paths that inhibit emergent teamwork.

**Open source tools reduce the barrier to entry for innovators.** Open source software, when developed professionally, can provide high quality, innovative, systems at lower costs by sharing efforts across organisations.