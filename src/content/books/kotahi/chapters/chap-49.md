---
title: "Glossary"
class: "component-body  chapter  "
order: 49
---

**AnyStyle** - An open source citation parsing tool that identifies reference strings and matches them to citation metadata. Developed by Coko.

**API** - Application Programming Interface. Enables software to exchange data and interact with Kotahi programmatically.

**Archetype** - A predefined configuration or template for workflows and publishing models in Kotahi.

**Automatic ingestion** - The automated importing of content into a system. Kotahi can be configured to automatically ingest manuscripts from sources like preprint servers.

**arXiv** - A popular preprint repository used across many scientific disciplines.

**bioRxiv** - A preprint repository for biology, operated by Cold Spring Harbor Laboratory.

**Collaborative review** - Multiple reviewers work together, either openly or anonymously, to provide a consolidated set of feedback.

**Community review** - Self-organisation of reviewers from the community, often used for post-publication review of preprints.

**Coko** - A non-profit building open source publishing infrastructure to enable new models of research communication.

**Cokoserver** - An open source workflow server by Coko that integrates tools and manages publishing operations.

**Containers** - A standardised unit of software encapsulating code and dependencies. Kotahi uses Docker containers.

**Concurrency** - The execution of multiple tasks or processes at the same time. In publishing, it refers to authors, editors, designers, etc. working on a project simultaneously.

**Crossref** - A nonprofit collaborative reference linking service that provides persistent identifiers (DOIs) for scholarly research.

**Curation** - the process of selecting, organising, and presenting content to enhance its value, context, and accessibility for specific audiences.

**Deployment** - The process of installing, configuring, and running an application like Kotahi on servers to make it available for use.

**Docker** - An open platform for developing, shipping, and running applications using containers. Used to package and deploy Kotahi's microservices.

**DOI (Digital Object Identifier)** - A unique persistent identifier for digital objects like journal articles. Provided by identification systems like Crossref.

**Downconversion** - Converting a structured document format to a less structured one, usually involving some loss of information. E.g. HTML to plain text.

**Double-blind review** - The identities of both authors and reviewers are concealed from each other throughout the review process.

**Evaluation** - The process of assessing a manuscript or preprint submission through peer review and editorial decision-making.

**Express.js** - A lightweight Node.js web application framework used in Kotahi for routing, middleware, and APIs.

**FLAX** - An open source static site generator and publishing framework developed by Coko, built on 11ty. FLAX is integrated into Kotahi.

**Form builder** - A tool for creating customised forms to collect submission metadata in Kotahi. Enables optimising submission workflows.

**Fragmented Publishing Process (FPP)** - A disjointed publishing process involving different teams, tools, and file formats between the content creation and production stages.

**Git** - A distributed version control system for tracking code changes. Kotahi uses GitLab for repository management.

**Group** - In Kotahi, an isolated publishing community like a journal, preprint server, or review collection. Groups act as independent tenants.

**Instance** - A single installed copy of the Kotahi software. Can contain multiple groups running independently.

**JATS (Journal Article Tag Suite)** - An XML-based standard format for structuring and encoding scholarly journal articles to enable archiving, exchange, and preservation.

**JavaScript** - A programming language commonly used for web development. Kotahi is built primarily using JavaScript technologies.

**Manuscript** - A written document containing research findings submitted for publication in a journal.

**Metadata** - Data about a publication, like title, authors, keywords. Required for identification, discovery, and management.

**Micropublication** - Bite-sized scholarly outputs focused on a single finding or idea, enabled by web-based publishing.

**Microservices** - An architectural approach in which an application is composed of small independent services that work together. Kotahi employs a microservices architecture.

**Multitenancy** - An architecture where a single software instance serves multiple independent groups like journals or preprint communities.

**Node.js** - An open source JavaScript runtime environment for building server-side applications. Kotahi's backend services are built using Node.js.

**Notifications** - Emails, in-app messages, and alerts to notify users of events and updates.

**Open review** - Reviewers and authors are known to each other during the review process. Reviews may be published alongside articles.

**Open Source** - Software with source code that anyone can inspect, modify, and enhance.

**Paged.js** - An open source JavaScript library for pagination and print-quality PDF generation from HTML. Developed by Coko.

**Paged Media** - An evolving CSS standard for styling and laying out content in paged environments like PDF documents. Enables print-quality typesetting from HTML.

**Postgres** - An open source relational database system used by Kotahi to store content and metadata.

**Preprint** - A manuscript that is shared publicly prior to formal peer review and publication in a journal.

**Preprint servers** - Online repositories for sharing preprint manuscripts, like arXiv, bioRxiv.

**Preprint review** - the process of assessing and evaluating scientific preprint articles (usually) prior to formal publication in a journal.

**Production tools** - Components of a publishing system that prepare content for final publication, like typesetting and PDF generation.

**Progressive structuring** - The ability to incrementally add more structure to a document format without interrupting the workflow.

**Publish-Review-Curate (PRC)** - An emerging publishing model based on posting preprints, community peer review, and curation.

**Real-time editing** - A feature allowing multiple users to collaboratively edit a document at the same time and see live updates, such as in in Google Docs.

**React** - An open source JavaScript library for building user interfaces. Used extensively in Kotahi for UI components.

**Research object** - A generalised term in Kotahi referring to any submitted item, including articles, data, code, presentations etc.

**Review rounds** - Iterative stages of peer review in which a submission may go through multiple cycles of assessment, revision, and re-review. After each round, the editor makes a decision to accept, reject, or initiate another review cycle.

**Rolling submissions** - Authors can update submissions during an active review round if enabled (this has proven useful for some preprint review use cases).

**PID (Persistent Identifier)** - a long-lasting reference to a digital resource that does not change over time. PIDs like DOIs allow reliably locating content even if it moves or changes hosts. Using PIDs enables consistent access and integrity in linking between related research objects across systems.

**Scalability** - Ability of a system to handle increased usage and data volume. Kotahi scales using microservices and containers.

**Sciety** - A platform built by eLife to aggregate preprint peer reviews happening across the web and power PRC workflows.

**Single-blind review** - Reviewers know the author's identity but authors do not know the reviewer identities.

**Single Source Publishing (SSP)** - Utilising a single file format throughout both content creation and production stages of publishing.

**Structured text** - A simple markup language like Markdown or AsciiDoc that adds some basic structure for formatting documents.

**Submission** - The process of submitting an article or other research object for publication or review in Kotahi.

**Taxonomy** - A hierarchical system for categorising and organising knowledge.

**Tenant** - An independent group within a shared Kotahi instance, like a journal or preprint server community. Multi-tenancy allows supporting many groups on one platform.

**Typesetting** - Adapting content styling and layout for different mediums like print or mobile screens.

**Upconversion** - Adding structural information to enhance a document's fidelity, like HTML to XML.

**Versioning** - Tracking revisions to submissions by creating new iterations or ‘versions’ recording the changes.

**Wax** - An open source browser-based rich text editor for scholarly writing and publishing, developed by Coko.

**Workflow** - The end-to-end set of tasks, roles, and steps involved in a publishing process.

**XML-first** - A single source publishing approach involving all users working directly in XML files and tools.

**XSLT** - A language for transforming XML documents into other formats like HTML. USed in XSweet.

**XSweet** - An open source tool by Coko for converting Microsoft Word documents to HTML.