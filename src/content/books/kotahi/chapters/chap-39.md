---
title: "Understand Your Workflow"
class: "component-body  chapter  "
order: 39
---

At a high level, many scholarly workflows follow a similar path - a research object is submitted, reviewed, improved, and shared. However, when examining the specifics, workflows that appear identical on the surface often diverge in their details. For example, while two projects may both conduct open peer review, one may rely on assigned editors while the other utilizes a flat (or flat-ish) community model. Or while two preprint review projects both automatically ingest submissions, their triage processes may differ significantly.

It's common for teams to state their needs are "quite simple" or their workflows are "pretty standard." But frequently, drilling down reveals unique requirements in submission steps, review processes, content types, and more. Standardized stages like "submit, review, improve, share" manifest differently across diverse fields and models. Assumptions of simplicity can obscure the intricacies involved in scholarly communication.

Thus it's important to resist perceived commonality and invest time to uncover granular needs. Workflow innovations often emerge from those subtleties. While patterns exist at a high level, the "magic" lies in the details.

The following is a self-help guide for understanding your current (if it exists) workflow and your ideal workflow.

**Step 1: Understand the current workflow** - this is necessary to generate an understanding of what you are actually doing right now. This step needs to drill down into the nitty-gritty of the daily workflow so you can get a complete picture of what actually happens ‘on the floor’. High-level executive summaries are not what we are after. We need to know what actually happens.

Without understanding exactly what you are doing now, improving what you do will be based on false assumptions. As poet Suzy Kassem has said:

> “Assumptions are quick exits for lazy minds that like to graze out in the fields without bother.”

When examining workflow we need to bother people. To ask hard questions and get to the bottom of things. I’ve often heard folks say they know their workflow or that someone within their organisation knows the workflow. But after asking a lot of exploratory questions I inevitably find that no one person understands exactly what happens at all stages of the workflow. Consequently, understanding your current workflow, to the granularity required to move to the next steps, requires a process in itself. But we will discuss this in more detail in the next article.

However, Step 1, while important (and important to get right), is just prep for the most critical step of the entire process:

**Step 2: Optimise the workflow** - optimising the workflow is what every publishing technology project should aim to do. If you don’t optimise the workflow, if you haven’t improved what you do, then what have you achieved? It would make sense then, to prioritise thinking about improving workflow before you start to think about technology.

![](/images/ec8e6e04e4fd_medium.jpeg)

Optimising the workflow is, in itself, a design problem and requires dedicated effort. Unfortunately this step is often skipped or paid only a passing tribute. Conflating steps 2 and 3, for example, is typical of the popular publishing RFP (Request for Proposals) process (see [https://upstream.force11.org/can‑we‑do‑away‑with‑rfps/](https://upstream.force11.org/can-we-do-away-with-rfps/)) :

1.  Understand the current (or speculative) workflow.
    
2.  Design technology to enable the workflow,
    
3.  Build (or choose) a new technology (optional).
    

An RFP process may be motivated by a few big ticket items that may need improving (or licensing issues as per above), but often lacks a comprehensive design process aimed at optimising the workflow. Instead, technology is chosen because it fulfills the requirements of the current workflow audit (Step 1) with a few possible fixes - this is because most RFP processes focus on updating technology rather than improving publishing processes. Consequently there are minimal, if any, workflow gains and a failed opportunity to identify many optimisations that can deeply affect a good technology choice and improve how you publish.

To avoid these problems and to improve how you publish, it is really necessary to focus dedicated design effort to optimising workflow.

The kind of process I’m talking about was once brilliantly described by Martin Thompson (Australian Network for Art and Technology) a long time ago. Martin was explaining the principles of video compression for streaming media, but the principles can be applied to workflow optimisation. Imagine a plastic bag full of air with an object in it (let’s say a book). The air represents all the time and effort required to produce that book. Now suck out the air, and we see the bag and book remain the same but the volume of air has reduced. We have reduced the effort and time to produce the book, while the book remains unchanged.

![](/images/7da4d66fa445_medium.jpeg)

This is a simple but clear metaphor for what we are trying to achieve. In summary, optimising workflow is about saving time and effort while not compromising the end product.

Workflow optimisation requires an examination of every stage of the process (as discovered in step 1 of our workflow-first process) and asking if that stage can be improved or eliminated. We are also asking some other questions such as -

*   Can anything be improved to help later stages be more efficient?
    
*   Are there ‘eddies’ in a workflow that can be fixed?
    
*   Can operations occur concurrently rather than sequentially?
    

When asking these questions, we will discover many efficiencies and we can slowly start drawing out a complete improved workflow. Some steps will disappear, some will be improved by mildly altering the order of operations, others will be improved by small tweaks. Some changes are more significant and may require greater change.

Once we have been through every stage looking for efficiencies, once we have our complete and optimised workflow, we are ready to start thinking about what we need to enable these changes. Technology may be part of the answer, but it is never the only answer, and in some cases it is not the answer at all. If we do decide new technology is required then only now should we start on the path to designing or choosing the tools to support our optimised workflow.