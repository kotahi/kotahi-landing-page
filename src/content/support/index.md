---
title: Flexibility, reliability and strong support
menu: Support
layout: single.njk
order: 500
intro: "Kotahi is committed to building shared solutions that evolve in line with the needs of diverse publishers."
---


## A network of trusted service providers

At Kotahi, we believe in giving publishers the freedom to choose the service providers that work best for them. That's why we welcome all kinds of providers who can support publishing. Whether you need help with installation, hosting, maintenance, feature development, or customer support, we want you to have access to the services that meet your specific needs. That's why we've partnered with industry leaders like Amnet and Cloud68 to offer you a hosted solution with top-notch service and support. With Kotahi and our trusted partners, you can focus on what you do best - publishing great content.

## Simplified migration

Migrating your archive of content to a new system can be a daunting task, but with Kotahi it doesn't have to be. Our platform was built with a diverse range of use cases in mind, meaning we can customize the core data structures to fit your specific needs. This minimizes the time spent converting, checking, and reprocessing your archive.

Kotahi also uses conventions that allow for the import and export of popular publishing formats such as JATS, XHTML, EPUB, PDF, and DOCX, making it easier to maintain compatibility with existing archives and standards. Trust us to make the migration process as pain-free as possible.

## The freedom to customise

That's correct! Kotahi is designed to be highly customizable, with the ability to adjust various aspects of the platform to fit specific needs. Its modular architecture also allows for the easy addition or replacement of services. Additionally, Kotahi utilizes international standards such as XHTML and JATS while also allowing for extensible technology to be added or overridden as needed.

## A modern, flexible, end-to-end solution for  publishing

The Kotahi open-source platform helps publishers take an optimized, modern approach to everything they do. Simplify your approach to submission and peer review. Optimise your production workflow. Host and present clear, engaging content on any device. Get clear reports and insights from your publishing program. 

With Kotahi, publishers can stay ahead of the curve and deliver the best possible experience for their authors and readers.

## Global team to meet global needs

Kotahi's team is committed to helping publishers with any questions or issues they may encounter while using the platform. Our team is distributed across multiple time zones, so we can provide support to publishers no matter where they are located. We are available through various channels, including email, text, and video conferencing. Additionally, we are constantly monitoring and improving the system to ensure that it is running smoothly and efficiently.

## Join the dicussion

Visit the Kotahi website to find information or contact our team for support. If you would like to join the Kotahi discussion, click [here](https://mattermost.coko.foundation/).
