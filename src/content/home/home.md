---
layout: home.njk
class: home
title: Kotahi
subtitle: The Next Generation Scholarly Publishing Platform
permalink: /index.html
---

### One Flexible, Customisable Platform for Journals, Preprint Servers, Review Communities, Micropublications. 
