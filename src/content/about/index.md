---
layout: about.njk
class: about
title: A better way to publish
permalink: /about/
menu: About
order: 001
# permalink: /index.html
features:
  - name: Compliant
    text: >-
      #### Kotahi meets the latest publishing standards.


      Kotahi evolves with publishing standards. Our workflows meet the latest requirements for discoverability, integration, and accessibility. We optimize for modern compliance so you can focus on content. With Kotahi, trust your system adheres to current best practices.
    avatar: /static/images/uploads/kotahi-upToDate.svg
    cta: []
  - name: Interoperable
    text: >-
      #### Kotahi can grow with you.


      Kotahi grows with you. We use open standards so your content integrates easily with third-party services for payments, analytics, multimedia, and more. Our pluggable architecture simplifies adding new capabilities like AI tools or virtual conferencing. Kotahi interoperates now and into the future.
    avatar: /static/images/uploads/kotahi-api.svg
  - name: Open
    text: >-
      #### Kotahi uses open-source technologies and is optimised for open.


      We are committed to being open in everything that we do. Our system can work with all content types including preprints, journal articles, datasets, and more. All content is fully accessible to data mining and indexing services, and our workflows and content are optimized for publishing to the web and third-party services like Crossref, PubMed, BioRvix, and Sciety.
    avatar: /static/images/uploads/kotahi-openSource.svg
  - name: Intuitive
    text: >-
      #### Kotahi works with you and your community, everywhere they are.


      Our system is designed to enhance the researcher and publisher experience. We offer a seamless user experience with an easy-to-use interface and optimized layouts that are designed for readability and accessibility.
    avatar: /static/images/uploads/kotahi-browser.svg
  - name: Innovative
    text: >-
      #### Kotahi solves problems other publishing systems can’t solve.


      Our architects deeply understand publishing workflows to build robust, user-centric software. We design quality into every feature to enhance publishing. Our rigorous approach solves problems at their root. The result - a system optimized end-to-end for modern publishing.
    avatar: /static/images/uploads/kotahi-innovation.svg
  - name: Built by the Coko Community
    text: >-
      #### We believe in the power of collaboration.


      We're proud to bring together multiple organizations and stakeholders who share our vision of building an open-source publishing toolset. Our community includes eLife, Amnet, BioPhysics CoLab, Sciety, Novel Coronavirus Research Compendium (NCRC), and many others. Together, we're working to create a publishing system that is flexible, scalable, and accessible to all. Join us in our mission and become part of Coko Community today.
    avatar: /static/images/uploads/kotahi-community.svg
  - name: Kotahi by Coko
    text: Transform your publishing workflows.  Switch to Kotahi by Coko
    avatar: /static/images/uploads/kotahi-workflow.svg
    cta:
      - url: mailto:adam@coko.foundation
        text: Email Coko!
Whitepaper: <p></p>
---


## One flexible, customisable platform for journals, preprint servers, preprint review, and micropublications

Kotahi is a state-of-the-art scholarly publishing platform developed by Coko. Our open source software supports multiple end-to-end workflows making it the perfect solution for publishers large and small. We offer smooth data migrations, swift handling of feature requests, and hands-on support to ensure that the platform is configured to meet your needs.


Kotahi will transform your publishing workflows. Here’s how...
