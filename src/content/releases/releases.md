---
title: "Releases"
permalink: "/releases/"
layout: single.njk
class: releases
menu: Releases
order: 800
intro: Release source code and further formation for each release can be found here.
---
<table>
<tr><td colspan="2" style='text-align: left;'><em>Quick Info</em></td></tr>
<tr><td>License:</td><td style='text-align: left;'>MIT</td></tr>
<tr><td>
Code:</td><td style='text-align: left;'><a href='https://gitlab.coko.foundation/kotahi/kotahi' target='_new'>https://gitlab.coko.foundation/kotahi/kotahi</a></td></tr><tr>
<td>Chat:</td><td style='text-align: left;'><a href='https://mattermost.coko.foundation' target='_new'>https://mattermost.coko.foundation</a></td></tr><tr>
<td>Forum:</td><td style='text-align: left;'><a href='https://forum.kotahi.community' target='_new'>https://forum.kotahi.community</a></td></tr>
</table>


{% gitlabreleases 'https://gitlab.coko.foundation', '373', 'kotahi', 'kotahi' %}
