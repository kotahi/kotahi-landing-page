---
menu: Community
order: 300
layout: community.njk
title: Community
permalink: /community/
person:
  - name: Emily Gurley (US)
    quote: Using Kotahi saved our team hours of tedious and unnecessary work required to triage articles for review. Time was of the essence and no other solution catered to our needs.
    text: >-
      Professor of the Practice, Department of Epidemiology, Johns Hopkins Bloomberg School of Public Health. 
    avatar: /static/images/uploads/emily.jpg
    top: 0
    left: 0
  - name: Paul Shannon (UK)
    quote: eLife selected Kotahi for its flexible, innovative design, empowering publishers like eLife to thrive under the PRC model.
    avatar: /static/images/uploads/paul-shannon.jpeg
    text: >-
     Head of Technology and Innovation, eLife.
    top: 0
    left: 0
  - name: Venu Prasad Menon (India)
    quote: Kotahi provided the ideal foundation for constructing Nvcleus after extensive evaluation.
    text: >-
      Managing Director, Amnet Systems. 
    avatar: /static/images/uploads/index2.jpeg
    top: -20
    left: 0
  - name: Omo Oaiya (Nigeria)
    quote: Kotahi enables journals and preprint teams of any size to easily configure the workflow to suit their unique requirements and publish to multiple endpoints at the click of a button.
    text: >-
      Chief Strategy Officer, West and Central Research and Education Network (WACREN). 
    avatar: /static/images/uploads/index4.jpeg
    top: 0
    left: 0
  - name: John Chodacki (USA)
    text: >-
      Director, University of California Curation Center (UC3) at California Digital Library.
    avatar: /static/images/uploads/index3.jpeg
    top: 0
    left: 0
  - name: Lesley Anson (UK)
    text: >-
      Executive Director, Science Colab
    avatar: /static/images/uploads/201104_la_portrait.jpg
    top: 0
    left: 0
  - name: Silva Arapi (Albania)
    text: >-
      Co-founder, [Cloud68.co](http://cloud68.co/).
    avatar: /static/images/uploads/silva.jpeg
    top: 0
    left: 0
  - name: Ryan Dix-Peek (South Africa)
    avatar: /static/images/uploads/ryandix-peek_profile.png
    text: >-
      Project Manager, Kotahi
    top: 0
    left: 0
  - name: Ben Whitmore (New Zealand)
    avatar: /static/images/uploads/ben-suit-20201006-retouched.jpg
    text: >-
      Lead Developer, Kotahi.
    top: 0
    left: 0
  - name: Adam Hyde (New Zealand)
    avatar: /static/images/uploads/adam2.png
    text: >-
      Coko Founder and Manager
    top: 0
    left: 0
  - name: "Owen Iyoha (Nigeria) "
    avatar: /static/images/uploads/index5.jpeg
    text: >-
      CEO, Eko-Konnect Research and Education Initiative. 
    top: 0
    left: -1
  - name: Redon Skikuli (Albania)
    text: >-
      Co-founder, [Cloud68.co](http://cloud68.co/).
    avatar: /static/images/uploads/48024345951_67855a2235_c.jpg
    top: 0
    left: 0
  - name: Agathe Baëz (France)
    avatar: /static/images/uploads/agathe.jpg
    text: >-
      Designer, Kotahi.
    top: 0
    left: 0
  - name: Jure Triglav (Slovenia)
    text: >-
      Former Lead Developer, Coko
    top: 0
    left: 0
    avatar: /static/images/uploads/square.jpg
  - name: Sidorela Uku (Albania)
    avatar: /static/images/uploads/sidorelauku-500x500.jpg
    text: |-
      QA, Kotahi.
    top: 0
    left: 0
  - name: Yannis Barlas (Greece)
    avatar: /static/images/uploads/yannis-r.jpg
    text: >-
      Cokos Lead Developer.
    top: 0
    left: 0
---
